import numpy as np
import scipy.stats as st


def get_data(n, param, potential=st.norm(100,3)):
    '''
    Generate data for one experiment.
    '''
    pA = param['pg'][0]
    sxA, sxB = param['sigmag']
    q = potential.rvs(n)
    g = st.bernoulli(pA).rvs(n)
    wxA = st.norm(0, sxA).rvs(n)
    wxB = st.norm(0, sxB).rvs(n)
    
    x = np.zeros(q.shape)
    x[g == 1] = q[g == 1] + wxA[g == 1]
    x[g == 0] = q[g == 0] + wxB[g == 0]
    return q, x, g


def get_top(x, k=3):
    """
    Get indices of k-largest elements in array x.
    """
    return np.flip(np.argsort(x))[:k]



def curves_one_stage(data, a1s, alg, T=10):
    """
    Get (Q, a1) and (p1B, a1) curves for different datasets.
    """
    q, x, g = data
    n = len(q)
    pA_exp = []
    q_exp = []
    
    for a1 in a1s:
        nA = np.sum(g)
        I1, _ = alg(x, q, g, a1, a2=a1)
        pA_exp.append(g[I1].sum() / nA)
        q_exp.append(q[I1].mean())
    return a1s, np.array(q_exp), np.array(pA_exp)


def curves_two_stage(data, a1s,  alg, T=10):
    """
    Get (Q, a1) and (p1B, a1) curves for different datasets.
    """
    q, x, g = data
    n = len(q)
    a2 = a1s[0]
    n2 = np.ceil(n * a2).astype(int)

    pA_exp = []
    q_exp = []
    
    for a1 in a1s:
        nA = np.sum(g)
        I1, I2 = alg(x, q, g, a1, a2)
        pA_exp.append(g[I1].sum() / nA)
        q_exp.append(q[I2].sum() / n2)
    return a1s, np.array(q_exp), np.array(pA_exp)
