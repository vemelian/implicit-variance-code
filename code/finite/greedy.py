from .util import get_top
import numpy as np


def greedy(x, q, g, a1, a2):
    """
    Greedy Policy.
    """
    n = x.shape[0]
    n1 = np.ceil(n * a1).astype(int)
    n2 = np.ceil(n * a2).astype(int)
    I1 = get_top(x, n1)
    I2 = I1[get_top(q[I1], n2)]
    return I1, I2
