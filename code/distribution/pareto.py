from .distribution import Distribution


class Pareto(Distribution):
    
    def __init__(self, xm, alpha):
        super(Pareto, self).__init__()
        self._xm = xm
        self._alpha = alpha
    
    def mean(self):
        return self._xm * self._alpha / (self._alpha - 1)

    def var(self):
        return self._xm ** 2 * self._alpha / (self._alpha - 1) ** 2 / (self._alpha - 2)
        
    def supp(self):
        return (self._xm, self._xm * (self._epsilon)**(-1 / self._alpha))
    
    def pdf(self, x):
        return self._alpha * self._xm**self._alpha / x**(self._alpha + 1)
    
    def __str__(self):
        return "{}({}, {})".format(self.__class__.__name__, self._xm, self._alpha)
