import numpy as np


class Distribution(object):
    """
    Generic distribution class.
    """
    def __init__(self):
        self._epsilon = 1e-8
        
    def pdf(self, x):
        pass
    
    def supp(self):
        pass
    
    def mean(self):
        pass

    def var(self):
        pass
    
    def __repr__(self):
        return self.__str__()
