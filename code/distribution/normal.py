from  .distribution import Distribution
import numpy as np
import scipy.stats as st


class Normal(Distribution):
    
    def __init__(self, mu, sigma):
        super(Normal, self).__init__()
        self._mu = mu
        self._sigma = sigma
    
    def supp(self):
        delta = st.norm(self._mu, self._sigma).ppf(1 - self._epsilon)
        return  (self._mu - delta, self._mu + delta)
    
    def pdf(self, x):
        return 1 / np.sqrt(2 * np.pi * self._sigma**2) * np.exp(-(x - self._mu)**2 / (2 * self._sigma**2))
    
    def mean(self):
        return self._mu

    def var(self):
        return self._sigma ** 2
    
    def __str__(self):
        return "{}({}, {})".format(self.__class__.__name__, self._mu, self._sigma)
