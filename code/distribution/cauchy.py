from .distribution import Distribution
import numpy as np
from scipy import stats as st

class Cauchy(Distribution):

    def __init__(self, loc, scale):
        super(Cauchy, self).__init__()
        self._loc = loc
        self._scale = scale
        self.dist = st.cauchy(loc=loc, scale=scale)
        
    def supp(self):
        return (-30, 30)
    
    def pdf(self, x):
        return self.dist.pdf(x)
    
    def mean(self):
        return self.dist.mean

    def var(self):
        return self.dist.var
    
    def __str__(self):
        return "{}({}, {})".format(self.__class__.__name__, self._loc, self._scale)
       