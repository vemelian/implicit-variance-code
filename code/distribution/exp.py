from .distribution import Distribution
import numpy as np


class Exp(Distribution):

    def __init__(self, lam):
        super(Exp, self).__init__()
        self._lam = lam
        
    def supp(self):
        return (0, -np.log(self._epsilon) / self._lam)
    
    def pdf(self, x):
        return self._lam * np.exp(-self._lam * x)
    
    def mean(self):
        return 1 / self._lam

    def var(self):
        return 1 / self._lam ** 2
    
    def __str__(self):
        return "{}({})".format(self.__class__.__name__, self._lam)
       