import scipy.stats as st
from scipy.optimize import newton, fsolve
from scipy import integrate
from .stage1_general import stage1
from .stage2_general import stage2

import numpy as np
from scipy.integrate import quad


TOL = 1e-5

def get_optimal_threshold(param):
    """
    Optimal Policy
    """
    def _conditions(t):
        t1a, t1b, t2 = t
        return (stage1([t1a, t1b], param) - param['a1'],\
                stage2([t1a, t1b], t2, param) - param['a2'],\
                opt_cond([t1a, t1b], t2, param))
    tg1a, tg1b, t2 =  fsolve(_conditions, x0=(1, 1, 1), xtol=TOL)
    return [tg1a, tg1b], t2


def opt_cond(t1, t2, param):
    '''
    Derivative of u_2 wrt p1a.
    '''
    return _opt_condg(t1[0], t2, param, g=0) - _opt_condg(t1[1], t2, param, g=1)


def _opt_condg(t1g, t2, param, g=0):
    prior = param['prior']
    sigmag = param['sigmag'][g]
    a, b = prior.supp()
    def pq(q, t1, sigmag):
            return st.norm(q, sigmag).pdf(t1) * prior.pdf(q) 
    nom = quad(lambda q: (q - t2) * pq(q, t1g, sigmag), a=t2, b=b)[0]
    denom = quad(lambda q: pq(q, t1g, sigmag), a=a, b=b)[0]
    if denom  == 0:
        return nom * np.inf 
    else:
        return nom / denom
