import scipy.stats as st
from scipy.optimize import newton, fsolve
from scipy import integrate
from .stage1_general import stage1, stage1g
import numpy as np


TOL = 1e-6

def get_greedy_threshold(param):
    '''
    Greedy policy.
    '''
    t1a, t1b =  fsolve(lambda t1: (stage1(t1, param) - param['a1'], greedy_condition(t1, param)),\
                         x0=(1, 1), xtol=TOL)
    return t1a, t1b


def greedy_condition(t1, param):
    t1a, t1b = t1
    return _greedy_cond(t1a, param, g=0) - _greedy_cond(t1b, param, g=1)


def _greedy_cond(t1, param, g=0):
    '''
    Auxillary function to calculate greedy thresholds.
    '''
    sigmag = param['sigmag']
    prior = param['prior']
    a, b = prior.supp()
    def pq(q, t1, sigmag):
        return st.norm(q, sigmag).pdf(t1) * prior.pdf(q) 
    nominator = integrate.quad(lambda q: q * pq(q, t1, sigmag[g]),
                               a=a, b=b, epsabs=TOL, epsrel=TOL)[0]
    denominator = integrate.quad(lambda q: pq(q, t1, sigmag[g]),\
                                 a=a, b=b, epsabs=TOL, epsrel=TOL)[0]
    if denominator == 0:
        return np.inf
    else:
        return nominator / denominator  
