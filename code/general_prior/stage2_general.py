import numpy as np
from scipy import stats as st
from scipy import integrate

TOL=1e-5

def stage2g(t1g, t2, param, g=0, e=0):
    """
    Calculate selection probability P(Y_2=1|g) 
    or expected quality if e=1.
    """
    prior = param['prior']
    sigmag = param['sigmag']
    a, b = prior.supp()
    return integrate.quad(lambda q: q**e * (1 - st.norm().cdf((t1g - q) / sigmag[g] )) * prior.pdf(q),\
                          a=t2, b=b, epsabs=TOL, epsrel=TOL)[0]

def stage2(t1, t2, param, e=0):
    """
    Calculate selection probability P(Y_2=1 | g) 
    or expected quality if e=1.
    """
    pg = param['pg']
    res = 0
    for i in range(2):
        res += stage2g(t1g=t1[i], t2=t2, param=param, g=i, e=e) * pg[i]
    return res