import numpy as np
from scipy import stats as st
from scipy import integrate
from scipy.optimize import newton, fsolve


def stage1g(t1g, param, g=0, e=0):
    '''
    Probability (or expected quality) of selection at 1st stage given group.
    Set e=1 to calculate the expected value of Q given selection Y_1=1. 
    '''
    sigmag = param['sigmag']
    prior = param['prior']
    a, b = prior.supp()
    return integrate.quad(lambda q: q**e * (1 - st.norm().cdf((t1g - q) / sigmag[g] )) \
        * prior.pdf(q), a=a, b=b, epsabs=1e-6)[0]


def stage1(t1, param, e=0):
    '''
    Total probability (or expected quality) of selection at 1st stage.
    Set e=1 to calculate the expected value of Q given selection. 
    '''
    pg = param['pg']
    budget1 = 0
    for i in [0, 1]:
        budget1 += pg[i] * stage1g(t1g=t1[i], param=param, g=i, e=e)
    return budget1
