from scipy.optimize import newton
from .stage1_general import stage1, stage1g


TOL = 1e-6

def get_single_threshold(param):
    '''
    Single threshold policy.
    '''
    t1 = newton(lambda t1: stage1([t1, t1], param, e=0) - param['a1'], x0=1, tol=TOL)
    return [t1, t1]


def get_beta_threshold(param, beta=0.8):
    '''
    Beta-rule.
    '''
    [t1, t1] =  get_single_threshold(param)
    p1a = stage1g(t1, param, g=0)
    p1b = stage1g(t1, param, g=1)
    pa, pb = param['pg']
    a1 = param['a1']
    if p1a / p1b <= beta:
        p1b = a1 / (pb + beta * pa)
        p1a = beta * p1b
    elif p1b / p1a <= beta:
        p1a = a1 / (pa + beta * pb)
        p1b = beta * p1a
    else:
        return [t1, t1]

    t1a = newton(lambda t1: stage1g(t1, param, g=0) - p1a, x0=1, tol=TOL)
    t1b = newton(lambda t1: stage1g(t1, param, g=1) - p1b, x0=1, tol=TOL)
    return [t1a, t1b]


def get_dp_threshold(param):
    '''
    Demographic Parity policy.
    '''
    t1s = []
    for g in [0, 1]:
        t1s.append(newton(lambda t1: stage1g(t1, param, e=0, g=g) - param['a1'], x0=1, tol=TOL))
    return t1s


def dp_condition(t1, param):
    """
    P(Y_1=1 | a) = P(Y_1=1 | b) condition.
    """
    t1a, t1b = t1
    return stage1g(t1a, param, e=0, g=0) - stage1g(t1b, param, e=0, g=1)
