import numpy as np
from .fair_policy import get_dp_threshold, get_single_threshold, get_beta_threshold
from .greedy_policy import get_greedy_threshold
from .optimal_policy import get_optimal_threshold
from .stage1_general import stage1g, stage1
from .stage2_general import stage2
from scipy.optimize import newton, brentq
from tqdm import tqdm, tqdm_notebook
import scipy.stats as st

TOL=1e-6

def u2_a1(param, inrange=None, heavy=False):
    """
    Calculate thresholds, utility and selection proba for different policies
    by varying alpha_1 and fixing alpha_2.
    """
    u1s = {"dp": [], "sin": [], "gr": [], 'opt': [], 'beta': []} # utilities at 1st
    u2s = {"dp": [], "sin": [], "gr": [], 'opt': [], 'beta': []} # utilities at 2nd
    t1s = {"dp": [], "sin": [], "gr": [], 'opt': [], 'beta': []} # thresholds at 1st
    t2s = {"dp": [], "sin": [], "gr": [], 'opt': [], 'beta': []} # thresholds at 2nd
    p1gs = {"dp": [], "sin": [], "gr": [], 'opt': [], 'beta': []} # selection probas given group
    if inrange is None:
        a1s = np.linspace(param['a2'] + 0.001, 0.999, 60)
    else:
        a1s = inrange
    for a1 in tqdm(a1s):
        param['a1'] = a1
        a2 = param['a2']
        
        t1 = {}
        t1['sin'] = get_single_threshold(param)
        t1['dp'] = get_dp_threshold(param)
        t1['beta'] = get_beta_threshold(param, beta=0.8)
        if heavy:
            t1['gr'] = get_greedy_threshold(param)
            t1['opt'], t2_opt = get_optimal_threshold(param)
        if heavy:
            policies = ['sin', 'dp', 'beta', 'opt', 'gr']
        else:
            policies = ['sin', 'dp']


        for policy in policies:            
            t1s[policy].append(t1[policy])
            a, b = param['prior'].supp()
            # use brent method to find a root
            # the search region is a support of prior PDF  
            if policy != 'opt':
                t2 = brentq(lambda t2: stage2(t1[policy], t2, param, e=0) - a2, a=a, b=b,  xtol=TOL)
            else:
                t2 = t2_opt
            t2s[policy].append(t2)
            p1gs[policy].append([stage1g(t1[policy][0], param, g=0, e=0), \
                                 stage1g(t1[policy][1], param, g=1, e=0)])
            u1s[policy].append(stage1(t1[policy], param, e=1))
            u2s[policy].append(stage2(t1[policy], t2, param, e=1))
    return a1s, u1s, u2s, t1s, t2s, p1gs
