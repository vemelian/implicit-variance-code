# Code from the paper "On Fair Selection in the Presence of Implicit Variance"

To generate the figures, the following ipynb files are used
* Sec. 2-3: figures-model.ipynb
* Sec. 3-4: figures1-stage-concave.ipynb, figures-1-2-stage.ipynb
* Sec 5.1: experiment-pareto.ipynb 
* Sec 5.2: experiment-jee.ipynb
* Sec 5.3: experiment-finite.ipynb
* Appendix A: appendix-different-prior.ipynb

The code for finite size selection is in "finite" folder, different distributions are implemented in "distribution".
The output data is stored in `data' folder.

# Requirements

* python3
* numpy
* pandas
* scipy
* matplotlib
* jupyter-notebook
